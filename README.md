# README #

Aplicatie pentru meniul de la firma. Si pentru incercat si studiat Cordova aplicand la ceva util.
Momentan meniul se obtine prin apel GET catre un fisier hostat in Google Drive.

### Setup ###

Pentru instalare, aici e ghidul, are si tutoriale, sunt mai multi pasi:

https://taco.visualstudio.com/en-us/docs/install-vs-tools-apache-cordova/

### Deploy ###

Pentru deploy pe Android va puteti ajuta de instructiunile lor de pe aici:
https://taco.visualstudio.com/en-us/docs/run-app-apache/#android-devices

### Contributii ###

Orice contributii sunt binevenite! Avem o lista deschisa de lucruri de facut. Cateva idei stranse pana acum (adaugati, va rog :) ):

* pastrare meniu in telefon (sa nu mai necesite net activ pentru a aduce meniul la fiecare afisare). Acum, meniul se obtine prin GET. Ar trebui: Salvare meniu + latest modification date, apoi la fiecare afisare apel HEAD pt obtinere Latest modification date (LMD) si comparare cu data meniului salvat, apoi GET in cazul in care exista un meniu mai nou + resalvare.

* widget Android (idee de la Mihai I.)

* salvare mancare de evitat; alerta cand urmeaza mancarea de evitat :)

* notificari zilnice cu meniul pe ziua curenta

* buton Like/Dislike

* centralizare preferinte pt mancaruri

* ... <orice idei noi aici>