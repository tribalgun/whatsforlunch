﻿/**
 * Current menu parser for JSON menu file
 **/
angular.module('starter.services').service('menuParser2', function () {
    this.parse = function (allMenus) {
        if (typeof (allMenus) === 'string') {
            allMenus = JSON.parse(allMenus);
        }

        var menus = []; //future menus
        var currentMenu; //menu for today

        var dates = Object.keys(allMenus);

        var today = new Date(), date, menuDate;
        for (var i = 0; i < dates.length; i++) {            
            date = dates[i];
            menuDate = new Date(date);
            var menu = {
                date: allMenus[date].dateDisplay,
                line1: allMenus[date].line1,
                line2: allMenus[date].line2,
                id: i
            };
            if (!currentMenu) {
                if (menuDate.getYear() >= today.getYear() && menuDate.getMonth() >= today.getMonth() &&
                    menuDate.getDate() >= today.getDate()) {
                    currentMenu = menu;
                }
                continue;
            }

            menus.push(menu);
        }

        if (!currentMenu && menus.length > 0) {
            currentMenu = menus.splice(0,1);
        }

        return {
            menus: menus,
            currentMenu: currentMenu
        };
    };

});