var enableCache = true;
var cacheFileName = "newCachedMenu3.json";

angular.module('starter.controllers', [])


.controller('MenuCtrl', function ($scope, $http, $location, menuCache, menuParser2) {
    $scope.data = {};
    $scope.data.currentPage;

    var setupSlider = function () {
        //some options to pass to our slider
        $scope.data.sliderOptions = {
            initialSlide: 0,
            direction: 'horizontal', //or vertical
            speed: 300 //0.3s transition
        };

        //create delegate reference to link with slider
        $scope.data.sliderDelegate = null;

        //watch our sliderDelegate reference, and use it when it becomes available
        $scope.$watch('data.sliderDelegate', function (newVal, oldVal) {
            if (newVal != null) {
                $scope.data.sliderDelegate.on('slideChangeEnd', function () {
                    $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
                    //use $scope.$apply() to refresh any content external to the slider
                    $scope.$apply();
                });
            }
        });
    };

    setupSlider();

    $scope.doRefresh = function () {
        console.log('Refreshing!');
        downloadMenu(function () {
            //Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        });
    }


    console.log('controller start');

    var storage = window.PERSISTENT;
    var fileName = cacheFileName;
    
    if (enableCache) {
        console.log('cache enabled! ', enableCache);
        loadMenu();
    } else {
        downloadMenu();
    }

    function loadMenu() {
        console.log('loading menu...');
        
        window.requestFileSystem(storage, 0, function (fs) { //TODO review the numbers
            console.log('file system open for reading: ' + fs.name);
            var dirEntry = fs.root;

            // Creates a new file or returns the file if it already exists.
            dirEntry.getFile(fileName, { create: false, exclusive: false }, function (fileEntry) {
                console.log("fileEntry exists1: " + fileEntry.isFile.toString() + ' ' + fileEntry.fullPath);
                // fileEntry.name == 'someFile.txt'
                // fileEntry.fullPath == '/someFile.txt'
                readFile(fileEntry);                
            }, fallbackToDownloadMenu);
        }, fallbackToDownloadMenu);
    }

    function readFile(fileEntry) {
        console.log('reading file...');
        fileEntry.file(function (file) {
            var reader = new FileReader();

            reader.onloadend = function () {
                console.log("Successful file read: " + this.result.length + ' ' + this.result.slice(0,100));

                var text = this.result;
                setMenu(text);
                downloadMenu();
            };

            reader.readAsText(file);

        }, onErrorReadFile);
    }

    function downloadMenu(afterDownloadFn) {
        menuCache.download().then(function successCallback(response) {
            setMenu(response.data);

            if (afterDownloadFn) {
                afterDownloadFn();
            }

            if (enableCache) {
                writeMenu(response.data);
            }
        }, function errorCallback(response) {
            console.error('Error on downloading menu...' + JSON.stringify(response));

            $scope.isReady = true;
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    function setMenu(responseData) {
        console.log('Setting menu...');

        var menuInfo = menuParser2.parse(responseData);

        $scope.menus = menuInfo.menus;
        $scope.currentMenu = menuInfo.currentMenu;
        $scope.isReady = true;
    }

    function writeMenu(dataObj) {
        window.requestFileSystem(storage, 0, function (fs) { //TODO review the numbers
            console.log('file system open for writing: ' +fs.name);
            var dirEntry = fs.root;

            dirEntry.getFile(fileName, {
                create: true, exclusive: false
            }, function (fileEntry) {

                console.log('Preparing to write file...');

                // Create a FileWriter object for our FileEntry
                fileEntry.createWriter(function (fileWriter) {

                    fileWriter.onwriteend = function() {
                        console.log("Successful file write...");
                    };

                    fileWriter.onerror = function (e) {
                        console.log("Failed file write: " +e.toString());
                    };

                    if (typeof (dataObj) === 'object') {
                        console.log('We will write a JSON object, keys no: ' + Object.keys(dataObj).length);
                        fileWriter.write(JSON.stringify(dataObj, null, 4));
                    } else {
                        console.log('We will write: ' + dataObj.length + ' ' + dataObj.slice(0, 100));
                        fileWriter.write(dataObj);
                    }
                });
            }, onErrorWriteFile);
        });
    }


    var onErrorWriteFile = function(err) {
        console.error('Error on write fs: ', err.message);
    }

    var onErrorLoadFs = function (err) {
        console.error('Error on load fs: ', err.message);
    }

    var onErrorGetFile = function (err) {
        console.error('Error on get file: ', err.message);
    }

    var fallbackToDownloadMenu = function (err) {
        console.error('An error has occurred while getting file from local storage: ', err.message);

        downloadMenu();
    }


    var onErrorReadFile = function (err) {
        console.error('Error on read: ', err.message);
    }
});


