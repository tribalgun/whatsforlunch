﻿/**
 * Obsolete menu parser for TXT menu file
 **/
angular.module('starter.services').service('menuParser', function () {
    this.parse = function (text) {
        var menus = []; //future menus
        var currentMenu; //menu for today

        var lines = text.split(/\r?\n/);
        var nonEmptyLines = [];

        var i;
        for (i = 0; i < lines.length; i++) {
            if (lines[i]) {
                nonEmptyLines.push(lines[i]);
            }
        }

        var today = new Date();
        for (i = 0; i < nonEmptyLines.length / 3; i++) {
            var menuDate = parseFormattedDate(nonEmptyLines[3 * i]);
            if (!menuDate) {
                continue;
            }
            var menu = {
                date: normalizeName(nonEmptyLines[3 * i]),
                line1: normalizeName(nonEmptyLines[3 * i + 1]),
                line2: normalizeName(nonEmptyLines[3 * i + 2]),
                id: i
            };
            if (!currentMenu) {
                if (menuDate.getYear() >= today.getYear() && menuDate.getMonth() >= today.getMonth() &&
                    menuDate.getDate() >= today.getDate()) {
                    currentMenu = menu;
                }
                continue;
            }

            menus.push(menu);
        }

        if (!currentMenu && menus.length > 0) {
            currentMenu = menus.splice(0,1);
        }

        return {
            menus: menus,
            currentMenu: currentMenu
        };
    };


    function normalizeName(str) {
        if (!str) {
            return;
        }

        str = str.replace(/^\(|\)$/g, ''); //get rid of starting and ending paranthesis
        if (!str || str.length <= 1) {
            return str;
        }
        return str[0].toUpperCase() + str.slice(1).toLowerCase();
    }

    function parseFormattedDate(text) { //LUNI – 5 DECEMBRIE   2016
        var tokens = text.split(/[^a-zA-Z0-9]/);
        var dow, day, month, year;
        for (var i = 0; i < tokens.length; i++) {
            if (tokens[i]) {
                if (!dow) {
                    dow = tokens[i];
                }
                else if (!day) {
                    day = tokens[i];
                } else if (month === undefined) {
                    month = parseMonth(tokens[i]);
                } else if (!year) {
                    year = tokens[i];
                }
            }
        }

        if (year && month !== undefined && day) {
            return new Date(year, month, day);
        }
    }

    function parseMonth(text) {
        switch (text.toLowerCase()) {
            case 'ianuarie': return 0;
            case 'februarie': return 1;
            case 'martie': return 2;
            case 'aprilie': return 3;
            case 'mai': return 4;
            case 'iunie': return 5;
            case 'iulie': return 6;
            case 'august': return 7;
            case 'septembrie': return 8;
            case 'octombrie': return 9;
            case 'noiembrie': return 10;
            case 'decembrie': return 11;
        }
    }

});