﻿angular.module('starter.services').service('menuCache', function ($http) {
    //extract these to a config file
    var enableCache = false;
    //var menuDownloadUrl = 'https://drive.google.com/uc?export=download&id=0B9-5FXpQtiXgbGU0NEluMXUxUGM'; //TXT - menuParser
    var menuDownloadUrl = 'https://drive.google.com/uc?export=download&id=1KYKL_gXotNdKfFfzBSnLBE9f9uSf9u8V'; //JSON - menuParser2

    /* TODO download and re-cache menu */
    this.download = function () {
        console.log('Downloading menu...');
        return $http({
            method: 'GET',
            url: menuDownloadUrl
        });
    };

    /* TODO load from cache; if nothing in cache, trigger download */
    this.load = function () { };
});